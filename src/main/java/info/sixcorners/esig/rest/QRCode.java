package info.sixcorners.esig.rest;

import static com.google.zxing.BarcodeFormat.QR_CODE;
import static com.google.zxing.client.j2se.MatrixToImageWriter.toBufferedImage;

import java.awt.image.BufferedImage;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import lombok.SneakyThrows;

import com.google.zxing.qrcode.QRCodeWriter;

@Path("qrcode")
public class QRCode {
	private static final QRCodeWriter qr = new QRCodeWriter();

	@GET
	@Produces("image/*")
	@SneakyThrows
	public BufferedImage genQRCode(@QueryParam("text") String text,
			@QueryParam("size") @DefaultValue("125") int size) {
		return toBufferedImage(qr.encode(text, QR_CODE, size, size));
	}
}
