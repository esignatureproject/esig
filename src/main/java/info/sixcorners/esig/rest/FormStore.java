package info.sixcorners.esig.rest;

import static info.sixcorners.esig.util.Util.getEmf;
import info.sixcorners.esig.data.FormData;
import info.sixcorners.esig.util.Email;

import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Path("formstore")
public class FormStore {
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@SneakyThrows
	public Response store(@BeanParam FormData data,
			@Context HttpServletRequest r) {
		@Cleanup
		val em = getEmf().createEntityManager();
		val tx = em.getTransaction();
		tx.begin();
		try {
			em.persist(data);
			tx.commit();
		} finally {
			if (tx.isActive())
				tx.rollback();
		}
		val sigId = URLEncoder.encode(data.getSigId(), "UTF-8");
		if (data.isSubmitSignatureChecked()) {
			return Response.seeOther(URI.create("scan.jsp?sigId=" + sigId))
					.build();
		} else if (data.isEmailPDF()) {
			String pdfLink = r.getScheme() + "://" + r.getServerName() + ":"
					+ r.getServerPort() + r.getContextPath() + "/pdf?sigId="
					+ sigId;
			try {
				Email.send(data.getEmail(), new URL(pdfLink));
			} catch (NullPointerException e) {
				log.error("Error sending email.", e);
			}
		}
		return Response.seeOther(URI.create("pdf?sigId=" + sigId)).build();

	}
}
