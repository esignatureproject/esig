package info.sixcorners.esig.rest;

import static info.sixcorners.esig.util.Util.getEmf;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import info.sixcorners.esig.data.FormData;
import info.sixcorners.esig.data.Point;
import info.sixcorners.esig.data.SigStorePostReq;
import info.sixcorners.esig.util.Util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.val;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

@WebServlet("/pdf")
public class PDFServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	@SneakyThrows
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		@Cleanup
		val em = getEmf().createEntityManager();
		val tx = em.getTransaction();
		tx.begin();
		FormData data;
		SigStorePostReq sigdata;
		try {
			data = em.find(FormData.class, req.getParameter("sigId"));
			sigdata = em.find(SigStorePostReq.class, req.getParameter("sigId"));
		} finally {
			tx.rollback();
		}
		if (data == null) {
			resp.sendError(SC_NOT_FOUND);
			return;
		}
		BufferedImage img = null;
		String formattedDate = "";
		if (sigdata != null) {
			val in = new ByteArrayInputStream(sigdata.getImageData());
			img = ImageIO.read(in);
			val op = Util.getRotationOp(-sigdata.getRotation(), img.getWidth(),
					img.getHeight());
			img = op.filter(img, null);

			Point lastPoint = getLastNonNullPoint(sigdata.getPoints());
			if (lastPoint != null) {
				formattedDate = getFormattedDate(lastPoint.getTime());
			}

		}
		resp.setContentType("application/pdf");
		resp.setHeader("Content-Disposition", "inline; filename=\"LoanApplication.pdf\"");
		createPdf(data.getFirstName(),
				data.getLastName(),
				data.getAddress(),
				data.getCity(),
				data.getState(),
				data.getZipCode(),
				data.getEmail(),
				data.getTelephone(),
				data.getLoanAmount(),
				img,
				formattedDate,
				resp.getOutputStream());
	}

	/**
	 * @param date The date to format.
	 * @return A String containing the date format "MM/dd/yyyy" for America/Chicago time.
	 * This format should accommodate Daylight Savings Time changes.
	 */
	private String getFormattedDate(Date date) {
		TimeZone timeZone = TimeZone.getTimeZone("America/Chicago");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		sdf.setTimeZone(timeZone);
		boolean dstIsInEffect = timeZone.inDaylightTime(date);
		return sdf.format(date) + " "
				+ sdf.getTimeZone().getDisplayName(dstIsInEffect, TimeZone.SHORT);
	}

	/**
	 * @param points The point array to search.
	 * @return Returns the last non-null point in the provided Point array if one exists.
	 * If the list does not contain a non-null point, null will be returned.
	 */
	private Point getLastNonNullPoint(Point[] points) {
		for (int i = points.length - 1; i > -1; i--) {
			if (points[i] != null) {
				return points[i];
			}
		}
		return null;
	}

	/**
	 * This method draws a title cell and a content cell onto the provided PDPageContentStream.
	 * The title cell will be drawn above the content cell.
	 * The cells will be the same width, but the content cell
	 * will be twice the height of the title cell.
	 * @param x The x coordinate for the title cell rectangle
	 * @param y The y coordinate for the title cell rectangle
	 * @param titleWidth The width of the title cell rectangle
	 * @param titleHeight The height of the title cell rectangle
	 * @param title The string to be placed in the title cell.
	 * @param content The string to be placed in the content cell.
	 * @param contentStream The PDPageContentStream for the cells to be drawn on.
	 * @throws IOException
	 */
	private void drawCells(int x, int y, int titleWidth, int titleHeight, String title,
			String content, PDPageContentStream contentStream) throws IOException {

		final int blankBoxWidth = titleWidth, blankBoxHeight = titleHeight * 2;

		contentStream.setNonStrokingColor(Color.LIGHT_GRAY);
		contentStream.fillRect(x, y, titleWidth, titleHeight); //draw title background

		contentStream.setNonStrokingColor(Color.BLACK);
		contentStream.addRect(x, y, titleWidth, titleHeight); //draw title border
		contentStream.addRect(x, y - blankBoxHeight, blankBoxWidth, blankBoxHeight); //draw blank box below
		contentStream.stroke();

		contentStream.beginText();
		contentStream.setFont(PDType1Font.TIMES_BOLD, 8);
		contentStream.moveTextPositionByAmount(x + 3, y + 2);
		contentStream.drawString(title); //draw title
		contentStream.endText();

		//draw content if it was retrieved.
		if (content != null) {
			contentStream.beginText();
			contentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
			contentStream.moveTextPositionByAmount(x + 3, y- 5 - titleHeight);
			contentStream.drawString(content);
			contentStream.endText();
		}

	}

	/**
	 * Draws a date String and date baseline onto the provided contentStream.
	 * @param x The x coordinate of the pdf to draw the date at.
	 * @param y The y coordinate of the pdf to draw the date at.
	 * @param formattedDate A String containing an already formatted date to be drawn.
	 * @param contentStream The PDPageContentStream of the pdf to draw on.
	 * @throws IOException
	 */
	private void drawDate(int x, int y, String formattedDate, PDPageContentStream contentStream)
			throws IOException {
		PDType1Font formattedDateFont = PDType1Font.TIMES_ROMAN;
		final int formattedDateFontSize = 12;
		final int lineWidth = 100;

		contentStream.beginText();
		contentStream.moveTextPositionByAmount(x, y);
		contentStream.setFont(formattedDateFont, formattedDateFontSize);
		contentStream.drawString("Date");
		contentStream.endText();

		x += 32; //shift right
		y -= 5; //shift down
		contentStream.setStrokingColor(Color.GRAY);
		contentStream.drawLine(x, y, x + lineWidth, y);

		if (formattedDate != null) {
			y += 5; //shift up
			float formattedDateWidth = formattedDateFont.getStringWidth(formattedDate)
					/ 1000 * formattedDateFontSize;
			final int formattedDateX = (int)((double)lineWidth / 2.0 - formattedDateWidth / 2.0);
			contentStream.beginText();
			contentStream.moveTextPositionByAmount(x + formattedDateX, y);
			contentStream.drawString(formattedDate);
			contentStream.endText();
		}

	}

	/**
	 * This method draws a signature title and places an image containing the user's 
	 * signature onto the PDPageContentStream.
	 * This method will shift the image up 75% of it's height to place the image's baseline in the
	 * correct position.
	 * This method assumes that all images in the database have a baseline drawn 75% on the way down the image.
	 * The provided signature's width will be set to 158 and it's
	 * height will be adjusted to maintain the aspect ratio.
	 * 158 has no significant meaning. It just seemed like a good size to use.
	 * If the provided image is null, a baseline will be drawn in it's place.
	 * @param x The x coordinate for the signature title to be drawn at.
	 * @param y The y coordinate for the signature title to be drawn at.
	 * @param image A BufferedImage containing the user's signature. null is a valid argument for the image.
	 * @param document The document that the signature image will be placed on.
	 * @param contentStream The PDPageContentStream of the pdf to place the image on.
	 * @throws IOException
	 */
	private void drawSignature(int x, int y, BufferedImage image,
			PDDocument document, PDPageContentStream contentStream) throws IOException {
		contentStream.beginText();
		contentStream.moveTextPositionByAmount(x, y);
		contentStream.drawString("Signature");
		contentStream.endText();

		y -= 5; // shift down

		//draw signature image if it was retrieved
		if (image != null) {
			PDXObjectImage ximage = new PDJpeg(document, image);

			//set the new width of the image making images from all devices display the same width.
			final double newWidth = 158;

			/*calculate the image's new height to go along with its new width
			 * while maintaining the original aspect ratio
			 */
			final double newHeight = newWidth * (double)ximage.getHeight() / (double)ximage.getWidth();

			x += 50; //shift right

			/*The image baseline should be 75% of the way down the image.
			 * Adding 75% of the image's height to the y coordinate should place the
			 * baseline of any image in the correct position.
			 */
			y += newHeight * .75; //shift image up 75% of its height

			//coordinates to draw a border for the image
			//float[] xs = {x -1, x + (float)newWidth + 1, x + (float)newWidth + 1, x -1};
			//float[] ys = {y + 1, y + 1, y - (float)newHeight - 1, y - (float)newHeight - 1};
			//contentStream.drawPolygon(xs, ys); //draw border

			contentStream.drawXObject(ximage, x, y - (int)newHeight, (int)newWidth, (int)newHeight);
		} else {
			x += 54; //shift right
			contentStream.setStrokingColor(Color.GRAY);
			contentStream.drawLine(x, y, x + 180, y);
		}
	}

	/**
	 * This method will create a PDF and write it to the ServletOutputStream.
	 * @param firstName A String containing a first name to be placed onto the PDF.
	 * @param lastName A String containing a last name to be placed onto the PDF.
	 * @param address A String containing an address to be placed onto the PDF.
	 * @param city A String containing a city to be placed onto the PDF.
	 * @param state A String containing a state to be placed onto the PDF.
	 * @param zipCode A String containing a zip code to be placed onto the PDF.
	 * @param emailAddress A String containing an email address to be placed onto the PDF.
	 * @param telephoneNumber A String containing a telephone number to be placed onto the PDF.
	 * @param loanAmount A String containing a loan amount to be placed onto the PDF.
	 * @param image An image containing the user's signature or null.
	 * @param formattedDate A String containing a formatted date to be placed onto the PDF.
	 * @param sos The ServletOutputStream to write the PDF to.
	 * @throws COSVisitorException
	 * @throws IOException
	 */
	private void createPdf(String firstName, String lastName, String address,
			String city, String state, String zipCode, String emailAddress,
			String telephoneNumber, String loanAmount, BufferedImage image,
			String formattedDate, ServletOutputStream sos)
					throws COSVisitorException, IOException {

		@Cleanup
		PDDocument document = new PDDocument();

		PDPage page = new PDPage();
		document.addPage(page);

		@Cleanup
		PDPageContentStream contentStream = new PDPageContentStream(document, page);

		final String title = "Loan Application";
		final PDType1Font titleFont = PDType1Font.TIMES_BOLD;
		final int titleFontSize = 25;
		final int padding = 50;
		float titleWidth = titleFont.getStringWidth(title) / 1000 * titleFontSize;
		final float pageWidth = page.getMediaBox().getWidth(); //== 612
		final float pageHeight = page.getMediaBox().getHeight(); //==792
		float titleX = (pageWidth / 2) - (titleWidth / 2);
		float titleY = pageHeight - padding;
		final int titleCellWidth = 256, titleCellHeight = 10;


		contentStream.beginText();
		contentStream.setFont(titleFont, titleFontSize);
		contentStream.moveTextPositionByAmount(titleX, titleY);
		contentStream.drawString(title); //draw title
		contentStream.endText();

		int x = padding, y = 700;
		drawCells(x, y, titleCellWidth, titleCellHeight, "FIRST NAME", firstName, contentStream);

		x += titleCellWidth; //shift right the width of the cells
		drawCells(x, y, titleCellWidth, titleCellHeight, "LAST NAME", lastName, contentStream);

		x -= titleCellWidth; //shift left the width of the cells
		y -= 30; //shift down the sum of the cell heights
		drawCells(x, y, titleCellWidth, titleCellHeight, "ADDRESS", address, contentStream);

		x += titleCellWidth; //shift right the width of the cells
		drawCells(x, y, titleCellWidth, titleCellHeight, "CITY", city, contentStream);

		x -= titleCellWidth; //shift left the width of the cells
		y -= 30; //shift down the sum of the cell heights
		drawCells(x, y, titleCellWidth, titleCellHeight, "STATE", state, contentStream);

		x += titleCellWidth; //shift right the width of the cells
		drawCells(x, y, titleCellWidth, titleCellHeight, "ZIP CODE", zipCode, contentStream);

		x -= titleCellWidth; //shift left the width of the cells
		y -= 30; //shift down the sum of the cell heights
		drawCells(x, y, titleCellWidth, titleCellHeight, "EMAIL ADDRESS", emailAddress, contentStream);

		x += titleCellWidth; //shift right the width of the cells
		drawCells(x, y, titleCellWidth, titleCellHeight, "PHONE NUMBER", telephoneNumber, contentStream);

		x -= titleCellWidth; //shift left the width of the cells
		y -= 30; //shift down the sum of the cell heights
		drawCells(x, y, titleCellWidth, titleCellHeight, "LOAN AMOUNT", loanAmount, contentStream);

		y -= 130; //shift down

		drawSignature(x, y, image, document, contentStream);

		x += 355; //shift right

		drawDate(x, y, formattedDate, contentStream);

		contentStream.close();

		document.save(sos);
		document.close();
	}

}
