package info.sixcorners.esig.rest;

import static info.sixcorners.esig.util.Util.getEmf;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import info.sixcorners.esig.data.FormData;
import info.sixcorners.esig.data.SigStorePostReq;
import info.sixcorners.esig.util.Email;
import info.sixcorners.esig.util.Util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;

import lombok.Cleanup;
import lombok.SneakyThrows;
import lombok.val;

@Path("sigstore")
public class SigStore {
	@POST
	@Consumes(APPLICATION_JSON)
	@SneakyThrows
	public void saveSig(SigStorePostReq data,
			@HeaderParam("User-Agent") String userAgent,
			@HeaderParam("X-Forwarded-For") String xForwardedFor,
			@Context HttpServletRequest req) {
		data.setUserAgent(userAgent);
		data.setXForwardedFor(xForwardedFor);
		@Cleanup
		val em = getEmf().createEntityManager();
		val tx = em.getTransaction();
		tx.begin();
		try {
			em.persist(data);
			tx.commit();
		} finally {
			if (tx.isActive())
				tx.rollback();
		}
		FormData formData = em.find(FormData.class, data.getSigId());
		if (formData.isEmailPDF()) {
			String pdfLink = req.getScheme() + "://" + req.getServerName()
					+ ":" + req.getServerPort() + req.getContextPath()
					+ "/pdf?sigId=" + formData.getSigId();
			System.out.println("send email of " + pdfLink + " to "
					+ formData.getEmail());
			try {
				Email.send(formData.getEmail(), new URL(pdfLink));
			} catch (NullPointerException n) {
				System.err.println("Error sending email.");
				n.printStackTrace();
			}
		}
	}

	@GET
	@Produces(APPLICATION_JSON)
	public List<String> getList() {
		@Cleanup
		val em = getEmf().createEntityManager();
		val tx = em.getTransaction();
		tx.begin();
		try {
			return em.createQuery("SELECT sigId FROM SigStorePostReq",
					String.class).getResultList();
		} finally {
			tx.rollback();
		}
	}

	@GET
	@Path("{sigId}")
	@Produces(APPLICATION_JSON)
	public SigStorePostReq getJson(@PathParam("sigId") String sigId) {
		@Cleanup
		val em = getEmf().createEntityManager();
		val tx = em.getTransaction();
		tx.begin();
		try {
			val data = em.find(SigStorePostReq.class, sigId);
			if (data == null)
				throw new WebApplicationException(NOT_FOUND);
			return data;
		} finally {
			tx.rollback();
		}
	}

	@GET
	@Path("{sigId}")
	@Produces("image/*")
	@SneakyThrows
	public BufferedImage getImage(@PathParam("sigId") String sigId) {
		val data = getJson(sigId);
		val img = ImageIO.read(new ByteArrayInputStream(data.getImageData()));
		val op = Util.getRotationOp(-data.getRotation(), img.getWidth(),
				img.getHeight());
		return op.filter(img, null);
	}
}
