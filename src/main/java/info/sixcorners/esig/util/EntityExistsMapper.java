package info.sixcorners.esig.util;

import static javax.ws.rs.core.Response.Status.CONFLICT;

import javax.persistence.EntityExistsException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EntityExistsMapper implements
		ExceptionMapper<EntityExistsException> {
	public Response toResponse(EntityExistsException ex) {
		return Response.status(CONFLICT)
				.entity(ex.getMessage())
				.type("text/plain").build();
	}
}
