package info.sixcorners.esig.util;

import static javax.mail.Message.RecipientType.TO;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import lombok.val;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class Email {
	private static final PasswordAuthentication pa = new PasswordAuthentication(
			"esigproject2014@gmail.com", "SoftwareEngineering##");

	private Email() {
	}

	/**
	 * @throws NullPointerException
	 *             if it cannot access the url
	 */
	public static void send(String recipiantEmailAddress, URL url)
			throws MalformedURLException {
		val props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		val session = Session.getDefaultInstance(props,
				new Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return pa;
					}
				});

		try {
			val msg = new MimeMessage(session);
			msg.setFrom("esigproject2014@gmail.com");
			msg.setRecipients(TO, recipiantEmailAddress);
			msg.setSubject("Your Loan Application");

			val textPart = new MimeBodyPart();
			textPart.setText("Your loan application is attached to"
					+ " this email in the form of a pdf.", "UTF-8");

			val pdfPart = new MimeBodyPart();
			pdfPart.setDataHandler(new DataHandler(url));
			pdfPart.setFileName("LoanApplication.pdf");

			msg.setContent(new MimeMultipart(textPart, pdfPart));
			Transport.send(msg);
			log.info("Mail Sent");
		} catch (MessagingException e) {
			log.error("Failed to send message", e);
		}
	}
}
