package info.sixcorners.esig.util;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class EntityNotFoundMapper implements
		ExceptionMapper<EntityNotFoundException> {
	public Response toResponse(EntityNotFoundException ex) {
		return Response.status(NOT_FOUND)
				.entity(ex.getMessage())
				.type("text/plain").build();
	}
}
