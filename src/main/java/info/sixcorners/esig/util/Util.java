package info.sixcorners.esig.util;

import static java.awt.image.AffineTransformOp.TYPE_NEAREST_NEIGHBOR;

import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.val;

public final class Util {
	private Util() {
	}

	@Getter(lazy = true)
	private static final AtomicReference<Object> emf = new AtomicReference<>();;

	/**
	 * TODO lombok's lazy getter didn't work so I manually implemented it.
	 */
	public static EntityManagerFactory getEmf() {
		Object value = emf.get();
		if (value == null) {
			synchronized (emf) {
				value = emf.get();
				if (value == null) {
					val actualValue = genEmf();
					value = actualValue == null ? emf : actualValue;
					emf.set(value);
				}
			}
		}
		return (EntityManagerFactory) (value == emf ? null : value);
	}

	@SneakyThrows
	private static EntityManagerFactory genEmf() {
		val props = new HashMap<String, String>();
		getConnection(props);
		val emf = Persistence.createEntityManagerFactory("db", props);
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				emf.close();
			}
		});
		return emf;
	}

	/**
	 * From https://archive.is/Y7Y8K
	 */
	private static void getConnection(HashMap<String, String> props)
			throws URISyntaxException {
		val DATABASE_URL = System.getenv("DATABASE_URL");
		if (DATABASE_URL == null)
			return;
		URI dbUri = new URI(DATABASE_URL);

		String[] split = dbUri.getUserInfo().split(":");
		String username = split[0];
		String password = split[1];
		String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':'
				+ dbUri.getPort() + dbUri.getPath();

		props.put("javax.persistence.jdbc.driver", "org.postgresql.Driver");
		props.put("javax.persistence.jdbc.url", dbUrl);
		props.put("javax.persistence.jdbc.user", username);
		props.put("javax.persistence.jdbc.password", password);
		// return DriverManager.getConnection(dbUrl, username, password);
	}

	public static AffineTransformOp getRotationOp(int rotation, int width,
			int height) {
		val tf = AffineTransform.getQuadrantRotateInstance(rotation);
		switch (rotation & 3) {
		case 0:
			tf.translate(0, 0);
			break;
		case 1:
			tf.translate(0, -height);
			break;
		case 2:
			tf.translate(-width, -height);
			break;
		case 3:
			tf.translate(-width, 0);
			break;
		}
		return new AffineTransformOp(tf, TYPE_NEAREST_NEIGHBOR);
	}
}
