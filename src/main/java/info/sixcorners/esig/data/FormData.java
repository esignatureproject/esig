package info.sixcorners.esig.data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;

import lombok.Data;

@Data
@Entity
public class FormData {
	@Id
	@FormParam("sigId")
	String sigId;

	@FormParam("first_name")
	private String firstName;
	@FormParam("last_name")
	private String lastName;
	@FormParam("address")
	private String address;
	@FormParam("city")
	private String city;
	@FormParam("state")
	private String state;
	@FormParam("zipCode")
	private String zipCode;
	@FormParam("email")
	private String email;
	@FormParam("telephone")
	private String telephone;
	@FormParam("loanamount")
	private String loanAmount;
	@FormParam("submit_signature")
	private boolean submitSignatureChecked;
	@FormParam("email_pdf")
	private boolean emailPDF;

	@HeaderParam("User-Agent")
	private String userAgent;
	@HeaderParam("X-Forwarded-For")
	private String xForwardedFor;
}
