package info.sixcorners.esig.data;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;

import lombok.Data;

@Data
@Entity
public class SigStorePostReq {
	@Id
	private String sigId;
	@Lob
	@Basic
	@NotNull
	private Point[] points;
	@Lob
	@Basic
	@NotNull
	private byte[] imageData;
	@NotNull
	private Date startTime, finishTime;
	private int width, height, rotation;

	@XmlTransient
	private String userAgent;
	@XmlTransient
	private String xForwardedFor;
}
