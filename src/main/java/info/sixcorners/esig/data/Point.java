package info.sixcorners.esig.data;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class Point implements Serializable {
	private static final long serialVersionUID = 1L;
	private int x, y;
	private Date time;
}
