if (!Date.now) {
	Date.now = function() {
		return new Date().getTime();
	};
}

window.addEventListener("orientationchange", resizeAndOrientate, false);
window.addEventListener("resize", resizeAndOrientate, false);

/* This function calls functions to resize the sign page so it does not take up 
 * the entire screen on large devices.
 * It also rotates the page so that the sign page's orientation is always landscape.
 * This function will not orientate properly with portrait monitors.
 */
function resizeAndOrientate() {
	if (window.innerWidth > window.innerHeight) {
		resizeLandscapeOrientation();
	} else {
		resizePortriatOrientation();
	}
	drawBaseline();
}

/* This function sets the sign page's orientation to portriat mode and resizes it so that
 * it does not take up the entire screen on large devices.
 * This function should be called when the user's device is in portriat mode.
 */
function resizePortriatOrientation() {
	var maxWidth = window.innerWidth;
	var maxHeight = window.innerHeight;

	// sets a limit on how tall the canvas and button div can be.
	if (maxHeight > 750) {
		maxHeight = 750;
		// maintain the ratio relative to the inner window size.
		maxWidth = maxHeight * (window.innerWidth / window.innerHeight);
	}

	var canvasWidth = 0.66 * maxWidth;
	var canvasHeight = 0.93 * maxHeight;
	var buttonDivHeight = canvasHeight;
	var buttonDivWidth = 0.24 * maxWidth;
	var verticalPadding = window.innerHeight - canvasHeight;
	var horizontalPadding = window.innerWidth - canvasWidth - buttonDivWidth;

	var buttonDivTop = verticalPadding / 2;
	var buttonDivLeft = horizontalPadding / 2;
	var canvasTop = buttonDivTop;
	var canvasLeft = buttonDivLeft + buttonDivWidth;

	var canvas = document.getElementById("canvas");
	canvas.style.position = "absolute";
	canvas.height = canvasHeight;
	canvas.width = canvasWidth;
	canvas.style.left = canvasLeft + "px";
	canvas.style.top = canvasTop + "px";

	var buttonDiv = document.getElementById("buttonDiv");
	buttonDiv.style.position = "absolute";
	buttonDiv.style.width = buttonDivWidth + "px";
	buttonDiv.style.height = buttonDivHeight + "px";
	buttonDiv.style.left = buttonDivLeft + "px";
	buttonDiv.style.top = buttonDivTop + "px";

	document.getElementById("clear").className = "portriatClear portriatButton";
	document.getElementById("submit").className = "portriatSubmit portriatButton";
}

/* This function sets the sign page's orientation to landscape mode and resizes it so that
 * it does not take up the entire screen on large devices.
 * This function should be called when the user's device is in landscape mode.
 */
function resizeLandscapeOrientation() {
	var maxWidth = window.innerWidth;
	var maxHeight = window.innerHeight;

	// sets a limit on how wide the canvas and button div can be.
	if (maxWidth > 750) {
		maxWidth = 750;
		// maintain the ratio relative to the inner window size.
		maxHeight = maxWidth * (window.innerHeight / window.innerWidth);
	}

	var canvasWidth = 0.93 * maxWidth;
	var canvasHeight = 0.66 * maxHeight;
	var buttonDivHeight = 0.24 * maxHeight;
	var verticalPadding = window.innerHeight - canvasHeight - buttonDivHeight;

	var canvasTop = verticalPadding / 2;
	var canvasLeft = (window.innerWidth / 2) - (canvasWidth / 2);
	var buttonDivTop = canvasTop + canvasHeight;

	var canvas = document.getElementById("canvas");
	canvas.style.position = "absolute";
	canvas.height = canvasHeight;
	canvas.width = canvasWidth;
	canvas.style.left = canvasLeft + "px";
	canvas.style.top = canvasTop + "px";

	var buttonDiv = document.getElementById("buttonDiv");
	buttonDiv.style.position = "absolute";
	buttonDiv.style.width = canvasWidth + "px";
	buttonDiv.style.height = buttonDivHeight + "px";
	buttonDiv.style.left = canvas.style.left;
	buttonDiv.style.top = buttonDivTop + "px";

	document.getElementById("clear").className = "landscapeClear landscapeButton";
	document.getElementById("submit").className = "landscapeSubmit landscapeButton";
}

/* This function draws a grey baseline on the canvas.
 * The line will be drawn 75% of the way down the canvas.
 * The line will be drawn from 5% of the width of the canvas from the left of the canvas 
 * to 95% of the width of the canvas from the left of the canvas.
 */
function drawBaseline() {
	var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");
	ctx.save();
	ctx.strokeStyle = "#BBB";
	ctx.lineWidth = 4;
	ctx.beginPath();
	if (window.innerWidth > window.innerHeight) { // draw horizontal
		ctx.moveTo(canvas.width * 0.05, canvas.height * 0.75);
		ctx.lineTo(canvas.width * 0.95, canvas.height * 0.75);
	} else { // draw vertical
		ctx.moveTo(canvas.width * 0.25, canvas.height * 0.05);
		ctx.lineTo(canvas.width * 0.25, canvas.height * 0.95);
	}
	ctx.stroke();
	ctx.closePath();
	ctx.restore();
}

window.addEventListener("load", function() {
	"use strict";
	resizeAndOrientate();
	var params = getParams();
	var canvas = document.getElementById("canvas");
	var ctx = canvas.getContext("2d");
	var drawing = false;
	var points = [];

	document.getElementById("clear").addEventListener("click", clear, false);
	var submit = document.getElementById("submit");
	submit.disabled = false;
	submit.addEventListener("click", function() {
		submit.disabled = true;
		submit.innerHTML = "Submitting...";
		var xhr = new XMLHttpRequest();
		xhr.open("POST", "sigstore");
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				if (xhr.status == 204) {
					alert("Signature successfully submitted");
				} else {
					alert("Error submitting signature");
				}
			}
			submit.disabled = false;
			submit.innerHTML = "Submit";
		};
		var rotation = 1;
		if (window.innerWidth > window.innerHeight)
			rotation = 0;
		xhr.setRequestHeader("Content-Type", "application/json");
		var imageData = canvas.toDataURL();
		imageData = imageData.substr(imageData.indexOf(',') + 1);
		var startTime = null, finishTime = null;
		for (var i = 0; i < points.length; i++)
			if (points[i] != null) {
				startTime = points[i].time;
				break;
			}
		for (var i = points.length - 1; i >= 0; i--)
			if (points[i] != null) {
				finishTime = points[i].time;
				break;
			}
		if (startTime == null)
			return;
		if (finishTime == null)
			return;
		xhr.send(JSON.stringify({
			sigId : params.sigId,
			points : points,
			imageData : imageData,
			startTime : startTime,
			finishTime : finishTime,
			width : canvas.width,
			height : canvas.height,
			rotation : rotation,
		}));
	}, false);

	canvas.addEventListener("pointerdown", start, false);
	canvas.addEventListener("pointerup", stop, false);
	canvas.addEventListener("pointerleave", stop, false);
	canvas.addEventListener("pointermove", move, false);

	function start(e) {
		e.preventDefault();
		ctx.lineWidth = 4;
		drawing = true;
		var rect = canvas.getBoundingClientRect();
		var point = {
			x : e.clientX - rect.left,
			y : e.clientY - rect.top,
			time : Date.now(),
		};
		points.push(point);
		// so that dots are recorded (clicks with no movement)
		ctx.beginPath();
		ctx.moveTo(point.x, point.y);
		ctx.lineTo(point.x + 1, point.y + 1);
		ctx.stroke();
		ctx.closePath();
		ctx.beginPath();
		ctx.moveTo(point.x, point.y);
	}

	function stop() {
		if (!drawing)
			return;
		drawing = false;
		ctx.closePath();
		points.push(null);
	}

	function move(e) {
		e.preventDefault();
		if (!drawing)
			return;
		var rect = canvas.getBoundingClientRect();
		var point = {
			x : e.clientX - rect.left,
			y : e.clientY - rect.top,
			time : Date.now(),
		};
		points.push(point);
		ctx.lineTo(point.x, point.y);
		ctx.stroke();
		// not closing the path makes it so the next touch
		// redraws all of the previous lines
		ctx.closePath();
		ctx.beginPath();
		ctx.moveTo(point.x, point.y);
	}

	function clear() {
		stop();
		points.length = 0;
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		drawBaseline();
	}

	function getParams() {
		var ret = {};
		var split = location.search.substring(1).split('&');
		for (var i = split.length - 1; i >= 0; i--) {
			var loc = split[i].indexOf('=');
			if (loc == -1) {
				ret[decodeURIComponent(split[i])] = null;
				continue;
			}
			var key = split[i].substring(0, loc);
			var value = split[i].substring(loc + 1);
			ret[decodeURIComponent(key)] = decodeURIComponent(value);
		}
		return ret;
	}
}, false);
