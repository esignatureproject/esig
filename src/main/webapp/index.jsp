<%@page pageEncoding="UTF-8"
	import="java.security.SecureRandom,java.math.BigInteger"
	trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<meta charset="UTF-8">
<title>Loan Application</title>
<h2>Loan Application</h2>
<form name="htmlform" action="formstore" method="post"
	accept-charset="UTF-8">
	<table>
		<tr>
			<td valign="top"><label for="first_name">First Name</label></td>
			<td valign="top"><input type="text" id="first_name" name="first_name"
				maxlength="50" size="30" required></td>
		</tr>

		<tr>
			<td valign="top"><label for="last_name">Last Name</label></td>
			<td valign="top"><input type="text" id="last_name" name="last_name"
				maxlength="50" size="30" required></td>
		</tr>
		<tr>
			<td valign="top"><label for="address">Address</label></td>
			<td valign="top"><input type="text" id="address" name="address"
				maxlength="50" size="30" required></td>
		</tr>
		<tr>
			<td valign="top"><label for="city">City</label></td>
			<td valign="top"><input type="text" id="city" name="city" maxlength="50"
				size="30" required></td>
		</tr>
		<tr>
			<td valign="top"><label for="state">State</label></td>
			<td valign="top"><select id="state" name="state" required>
					<option style="display: none;"></option>
					<option value="Alabama">Alabama</option>
					<option value="Alaska">Alaska</option>
					<option value="Arizona">Arizona</option>
					<option value="Arkansas">Arkansas</option>
					<option value="California">California</option>
					<option value="Colorado">Colorado</option>
					<option value="Connecticut">Connecticut</option>
					<option value="Delaware">Delaware</option>
					<option value="Florida">Florida</option>
					<option value="Georgia">Georgia</option>
					<option value="Hawaii">Hawaii</option>
					<option value="Idaho">Idaho</option>
					<option value="Illinois">Illinois</option>
					<option value="Indiana">Indiana</option>
					<option value="Iowa">Iowa</option>
					<option value="Kansas">Kansas</option>
					<option value="Kentucky">Kentucky</option>
					<option value="Louisiana">Louisiana</option>
					<option value="Maine">Maine</option>
					<option value="Maryland">Maryland</option>
					<option value="Massachusetts">Massachusetts</option>
					<option value="Michigan">Michigan</option>
					<option value="Minnesota">Minnesota</option>
					<option value="Mississippi">Mississippi</option>
					<option value="Missouri">Missouri</option>
					<option value="Montana">Montana</option>
					<option value="Nebraska">Nebraska</option>
					<option value="Nevada">Nevada</option>
					<option value="New Hampshire">New Hampshire</option>
					<option value="New Jersey">New Jersey</option>
					<option value="New Mexico">New Mexico</option>
					<option value="New York">New York</option>
					<option value="North Carolina">North Carolina</option>
					<option value="North Dakota">North Dakota</option>
					<option value="Ohio">Ohio</option>
					<option value="Oklahoma">Oklahoma</option>
					<option value="Oregon">Oregon</option>
					<option value="Pennsylvania">Pennsylvania</option>
					<option value="Rhode Island">Rhode Island</option>
					<option value="South Carolina">South Carolina</option>
					<option value="South Dakota">South Dakota</option>
					<option value="Tennessee">Tennessee</option>
					<option value="Texas">Texas</option>
					<option value="Utah">Utah</option>
					<option value="Vermont">Vermont</option>
					<option value="Virginia">Virginia</option>
					<option value="Washington">Washington</option>
					<option value="West Virginia">West Virginia</option>
					<option value="Wisconsin">Wisconsin</option>
					<option value="Wyoming">Wyoming</option>
			</select></td>
		</tr>
		<tr>
			<td valign="top"><label for="zipCode">Zip Code</label></td>
			<td valign="top"><input type="text" id="zipCode" name="zipCode"
				maxlength="50" size="30" required></td>
		</tr>
		<tr>
			<td valign="top"><label for="email">Email Address</label></td>
			<td valign="top"><input type="email" id="email" name="email" maxlength="80"
				size="30" required></td>
		</tr>
		<tr>
			<td valign="top"><label for="telephone">Telephone Number</label>
			</td>
			<td valign="top"><input type="text" id="telephone" name="telephone"
				maxlength="30" size="30" required></td>
		</tr>
		<tr>
			<td valign="top"><label for="loanamount">Loan Amount</label></td>
			<td valign="top"><input type="number" id="loanamount" name="loanamount"
				maxlength="30" size="30" min="1" required></td>
		</tr>
		<tr>
			<td valign="top"><label><input type="checkbox" name="submit_signature"
				value="true" checked>Submit Electronic Signature</label></td>
		</tr>
		<tr>
			<td valign="top"><label><input type="checkbox" name="email_pdf"
				value="true">Email me a copy of the PDF.</label></td>
		</tr>
	</table>
	<%!private static final SecureRandom rng = new SecureRandom();%>
	<% String sigId = new BigInteger(130, rng).toString(32); %>
	<input type="hidden" name="sigId" value="<%=sigId%>">
	<button type="submit">Submit</button>
</form>
