<%@page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>E-Signature</title>
	</head>

	<body>
	<%String sigId = request.getParameter("sigId");%>
	<%String signLink = request.getScheme()
			+ "://"
			+ request.getServerName()
			+ ":"
			+ request.getServerPort()
			+ request.getContextPath()
			+ "/sign.html?sigId="
			+ sigId;%>

	<h4>To enter your signature using your mobile device please use your device to scan the QR code below.</h4>
	<a href="<%=signLink%>">
		<img src="qrcode.png?text=<%=signLink%>" alt="QR Code to signature prompt">
	</a>

	<h4>Once you have submitted your signature, click the link below to view your information in a pdf.</h4>
	<a href="pdf?sigId=<%=sigId%>">View pdf</a>

	<br>
	<br>
	<br>

	<h4>If your mobile device does not have a QR code reader you can
		download a QR code reader from one of the links below.</h4>
	<a href="https://play.google.com/store/apps/details?id=com.google.zxing.client.android">
		<img alt="Android app on Google Play"
		src="https://developer.android.com/images/brand/en_app_rgb_wo_45.png" />
	</a>
	<a href="https://itunes.apple.com/us/app/barcodes-scanner/id417257150?mt=8">
		<img alt="Download on the App Store"
		src="https://linkmaker.itunes.apple.com/htmlResources/assets/en_us/images/web/linkmaker/badge_appstore-lrg.png">
	</a>
	</body>
</html>
